# Installs a whole stack of Graylog-Server

This ansible script will install all dependencies to run a Graylog-Server.

Homologated for **Ubuntu 20.04**.

## Softwares Versions

| Software      | Version |
| ------------- | ------- |
| Graylog       | 4.1.x   |
| Elasticsearch | 7.10.x  |
| OpenJDK       | 8       |
| MongoDB       | 4.4.x   |
| Nginx         | 1.18.x  |

## Requirements

```
pip3 install ansible passlib
```

## How to Use

- Change the variables in "group_vars/all.yml"
- Change the "hosts.yml" file in the "inventory" folder with the information of your server and run:

```
ansible-playbook playbook.yml --private-key=/path/for/your/key
```
